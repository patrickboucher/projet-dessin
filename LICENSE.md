<H1>LICENCE</H1>

Licence MIT-Expat

Copyright (c) 2020 Patrick Boucher

L'autorisation est accordée, gracieusement, à toute personne acquérant une copie<br>
de cette bibliothèque et des fichiers de documentation associés (la "Bibliothèque"),<br>
de commercialiser la Bibliothèque sans restriction, notamment les droits d'utiliser,<br>
de copier, de modifier, de fusionner, de publier, de distribuer, de sous-licencier<br>
et / ou de vendre des copies de la Bibliothèque, ainsi que d'autoriser les personnes<br>
auxquelles la Bibliothèque est fournie à le faire, sous réserve des conditions suivantes :

La déclaration de copyright ci-dessus et la présente autorisation doivent être<br>
incluses dans toutes copies ou parties substantielles de la Bibliothèque.

LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,<br>
EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,<br>
D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN<br>
AUCUN CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT<br>
RESPONSABLES DE TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ,<br>
QUE CE SOIT DANS LE CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN<br>
PROVENANCE DE, CONSÉCUTIF À OU EN RELATION AVEC LA BIBLIOTHÈQUE OU SON<br>
UTILISATION, OU AVEC D'AUTRES ÉLÉMENTS DE LA BIBLIOTHÈQUE.
