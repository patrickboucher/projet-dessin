# Projet dessin
### Description

Projet étudiant pour le cours de Programmation orientée objet 2 (420-3N1-DM),
Techniques de l'informatique du Cégep de Drummondville.

Ce programme permet de dessiner des formes simples dans un canevas et de 
sauvegarder les créations pour pouvoir les modifier plus tard. Les formes
peuvent être sélectionnées avec la souris pour ensuite être supprimées.

### Prérequis

[Oracle Java SE 14](https://www.oracle.com/java/technologies/javase/jdk14-archive-downloads.html)
ou
[OpenJDK 14](https://jdk.java.net/java-se-ri/14)

### Installation

1. Télécharger le dépôt git et extraire le fichier zip.
2. Compiler le projet :

    Windows 10 :
    ```
    javac -encoding utf8 -source-path src -d bin src\Application.java
    xcopy /I src\img\ bin\img\
    ```
    
    Ubuntu 18.04 :
    ```
    javac --source-path src -d bin src/Application.java
    cp -r src/img/ bin/
    ```

### Exécution

- Lancer le programme en mode graphique :

    Windows 10 :
    ```
    javaw -cp bin Application
    ```
  
    Ubuntu 18.04 :
    ```
    java -cp bin Application
    ```

### Auteur

Patrick Boucher

### Licence

[Licence MIT](https://gitlab.com/patrickboucher/projet-dessin/-/blob/master/LICENSE.md)
