import controleur.Controleur;
import modele.CanevasModele;
import vue.FenetrePrincipale;

/**
 * Lance le programme.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class Application {

    /**
     * Lance le programme.
     */
    public static void main(String[] args) {
        CanevasModele modele = new CanevasModele();
        FenetrePrincipale vue = new FenetrePrincipale(modele);
        new Controleur(modele, vue);
    }

}
