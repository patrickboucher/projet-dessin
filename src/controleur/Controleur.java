package controleur;

import modele.CanevasModele;
import modele.Persistance;
import modele.forme.Carre;
import modele.forme.Cercle;
import modele.forme.Ellipse;
import modele.forme.FigurePlane;
import modele.forme.Forme;
import modele.forme.Ligne;
import modele.forme.Rectangle;
import modele.forme.Triangle;
import vue.CanevasVue;
import vue.EnumMenu;
import vue.FenetrePrincipale;
import vue.IObservable;
import vue.IObservateur;
import vue.MenuContextuel;
import vue.MenuOutils;

import javax.swing.JButton;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collection;

/**
 * Contrôleur principal de l'application. Il observe les
 * {@link IObservable objets observables} et traite les données qu'il stocke
 * dans le {@link CanevasModele modèle}, selon les actions de l'utilisateur.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class Controleur implements IObservateur, MouseListener {

    /**
     * Le modèle qui contient les informations pour dessiner dans le canevas.
     */
    private CanevasModele canevasModele;

    /**
     * La fenêtre principale qui contient toutes les vues.
     */
    private FenetrePrincipale fenetrePrincipale;

    /**
     * Crée le contrôleur en assignant le {@link CanevasModele modèle} et la
     * {@link FenetrePrincipale fenêtre principale}.
     *
     * @param canevas Le modèle
     * @param fenetre La vue
     */
    public Controleur(CanevasModele canevas, FenetrePrincipale fenetre) {
        canevasModele = canevas;
        fenetrePrincipale = fenetre;
        observe(fenetre.getObservables());
        fenetre.getCanevas().addMouseListener(this);
    }

    /**
     * {@inheritDoc}
     *
     * @throws UnsupportedOperationException Si le traitement de la source n'a
     *                                       pas été implémenté.
     */
    @Override
    public void actualise(ActionEvent evenement, IObservable observable)
            throws UnsupportedOperationException {
        Class<?> source = observable.getClass();
        if (source == MenuOutils.class) {
            traiteEvenementMenuOutils(evenement);
        } else if (source == MenuContextuel.class) {
            traiteEvenementMenuContextuel(evenement);
        } else {
            throw new UnsupportedOperationException();
        }
        if (canevasModele.aChangeEtat()) {
            fenetrePrincipale.getCanevas().repaint();
        }
    }

    /**
     * Traite un événement produit d'un {@link MenuOutils}.
     *
     * @param evenement L'événement à traiter
     * @throws UnsupportedOperationException Si le traitement de l'événement n'a
     *                                       pas été implémenté.
     */
    private void traiteEvenementMenuOutils(ActionEvent evenement)
            throws UnsupportedOperationException {
        if (evenement.getSource() instanceof JButton) {
            MenuContextuel menu = MenuContextuel.getInstance();
            JButton bouton = (JButton) evenement.getSource();
            switch (bouton.getName()) {
                case "Carré" -> {
                    menu.affiche(EnumMenu.CARRE);
                }
                case "Rectangle" -> {
                    menu.affiche(EnumMenu.RECTANGLE);
                }
                case "Cercle" -> {
                    menu.affiche(EnumMenu.CERCLE);
                }
                case "Ellipse" -> {
                    menu.affiche(EnumMenu.ELLIPSE);
                }
                case "Triangle" -> {
                    menu.affiche(EnumMenu.TRIANGLE);
                }
                case "Ligne" -> {
                    menu.affiche(EnumMenu.LIGNE);
                }
                case "Sauvegarder" -> {
                    sauvegardeFormes();
                }
                case "Ouvrir" -> {
                    chargerFormes();
                }
                case "Supprimer sélection" -> {
                    canevasModele.retireSelection();
                }
                case "Supprimer tout" -> {
                    boolean supprimer = fenetrePrincipale.demandeConfirmation(
                            "Supprimer toutes les formes ?");
                    if (supprimer) {
                        canevasModele.retireTout();
                    }
                }
                default -> {
                    String message = "Le bouton '" +
                            bouton.getName() + "' n'est pas supporté";
                    throw new UnsupportedOperationException(message);
                }
            }
        }
    }

    /**
     * Traite un événement produit d'un {@link MenuContextuel}. Si l'événement
     * provient d'un bouton de validation, une forme est créée et stocké dans le
     * modèle.
     *
     * @param evenement L'événement à traiter
     * @throws UnsupportedOperationException Si le traitement de l'événement n'a
     *                                       pas été implémenté.
     */
    private void traiteEvenementMenuContextuel(ActionEvent evenement)
            throws UnsupportedOperationException {
        if (evenement.getSource() instanceof JButton) {
            Forme nouvelleForme;
            MenuContextuel menu = MenuContextuel.getInstance();
            JButton bouton = (JButton) evenement.getSource();
            switch (bouton.getName()) {
                case "carre appliquer" -> {
                    nouvelleForme = creeCarre();
                }
                case "rectangle appliquer" -> {
                    nouvelleForme = creeRectangle();
                }
                case "cercle appliquer" -> {
                    nouvelleForme = creeCercle();
                }
                case "ellipse appliquer" -> {
                    nouvelleForme = creeEllipse();
                }
                case "ligne appliquer" -> {
                    nouvelleForme = creeLigne();
                }
                case "triangle appliquer" -> {
                    nouvelleForme = creeTriangle();
                }
                default -> {
                    String message = "Le bouton '" +
                            bouton.getName() + "' n'est pas supporté";
                    throw new UnsupportedOperationException(message);
                }
            }
            menu.effaceChamps();
            nouvelleForme.setCouleur(menu.getCouleurForme());
            if (nouvelleForme instanceof FigurePlane) {
                FigurePlane figure = (FigurePlane) nouvelleForme;
                figure.setCouleurBordure(menu.getCouleurBordure());
                figure.setPlein(menu.formeEstSelectionnee());
                figure.setBordure(menu.bordureEstSelectionnee());
            }
            canevasModele.ajouteForme(nouvelleForme);
        }
    }

    /**
     * Crée un nouveau carré en utilisant les informations fournies par les
     * champs du {@link MenuContextuel}.
     *
     * @return Le nouveau carré
     */
    private Carre creeCarre() {
        MenuContextuel menu = MenuContextuel.getInstance();
        Point position = new Point(
                menu.getEntier("carre position x"),
                menu.getEntier("carre position y"));
        return new Carre(position, menu.getEntier("carre cote"));
    }

    /**
     * Crée un nouveau rectangle en utilisant les informations fournies par les
     * champs du {@link MenuContextuel}.
     *
     * @return Le nouveau rectangle
     */
    private Rectangle creeRectangle() {
        MenuContextuel menu = MenuContextuel.getInstance();
        return new Rectangle(
                menu.getEntier("rectangle position x"),
                menu.getEntier("rectangle position y"),
                menu.getEntier("rectangle longueur"),
                menu.getEntier("rectangle hauteur"));
    }

    /**
     * Crée un nouveau cercle en utilisant les informations fournies par les
     * champs du {@link MenuContextuel}.
     *
     * @return Le nouveau cercle
     */
    private Cercle creeCercle() {
        MenuContextuel menu = MenuContextuel.getInstance();
        return new Cercle(
                menu.getEntier("cercle position x"),
                menu.getEntier("cercle position y"),
                menu.getReel("cercle rayon"));
    }

    /**
     * Crée une nouvelle ellipse en utilisant les informations fournies par les
     * champs du {@link MenuContextuel}.
     *
     * @return La nouvelle ellipse
     */
    private Ellipse creeEllipse() {
        MenuContextuel menu = MenuContextuel.getInstance();
        return new Ellipse(
                menu.getEntier("ellipse position x"),
                menu.getEntier("ellipse position y"),
                menu.getEntier("ellipse longueur"),
                menu.getEntier("ellipse hauteur"));
    }

    /**
     * Crée une nouvelle ligne en utilisant les informations fournies par les
     * champs du {@link MenuContextuel}.
     *
     * @return La nouvelle ligne
     */
    private Ligne creeLigne() {
        MenuContextuel menu = MenuContextuel.getInstance();
        return new Ligne(
                menu.getEntier("ligne x1"),
                menu.getEntier("ligne y1"),
                menu.getEntier("ligne x2"),
                menu.getEntier("ligne y2"));
    }

    /**
     * Crée un nouveau triangle en utilisant les informations fournies par les
     * champs du {@link MenuContextuel}.
     *
     * @return Le nouveau triangle
     */
    private Triangle creeTriangle() {
        MenuContextuel menu = MenuContextuel.getInstance();
        return new Triangle(
                menu.getEntier("triangle x1"),
                menu.getEntier("triangle y1"),
                menu.getEntier("triangle x2"),
                menu.getEntier("triangle y2"),
                menu.getEntier("triangle x3"),
                menu.getEntier("triangle y3"));
    }

    /**
     * Sauvegarde les formes du modèle dans un fichier.
     * Affiche un message si une erreur est survenue.
     */
    private void sauvegardeFormes() {
        Persistance persistance = new Persistance();
        canevasModele.deselectionneTout();
        persistance.ecrire(canevasModele.getFormes());
        if (persistance.aMessage()) {
            fenetrePrincipale.affiche(persistance.getMessage());
        }
    }

    /**
     * Charge les formes dans le modèle depuis un fichier.
     * Affiche un message si une erreur est survenue.
     */
    @SuppressWarnings("unchecked")
    private void chargerFormes() {
        Persistance persistance = new Persistance();
        try {
            Collection<Forme> formes = (Collection<Forme>) persistance.lire();
            if (formes != null) {
                formes.forEach(forme -> canevasModele.ajouteForme(forme));
            }
            if (persistance.aMessage()) {
                fenetrePrincipale.affiche(persistance.getMessage());
            }
        } catch (ClassCastException exception) {
            String message = "Le fichier " + persistance.getNomFichier() +
                    " contient un objet inattendu.";
            System.err.println(message);
        }
    }

    /**
     * Traite un événement d'un clic de souris dans le canevas.
     *
     * @param evenement L'événement à traiter
     */
    @Override
    public void mouseClicked(MouseEvent evenement) {
        if (evenement.getSource().getClass() == CanevasVue.class) {
            canevasModele.selectionne(evenement.getPoint());
            if (canevasModele.aChangeEtat()) {
                fenetrePrincipale.getCanevas().repaint();
            }
        }
    }

    // Inutilisées de MouseListener :
    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

}
