package modele;

import modele.forme.FigurePlane;
import modele.forme.Forme;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Un modèle qui contient les informations des {@link Forme} à dessiner dans le
 * {@link vue.CanevasVue canevas}.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class CanevasModele {

    /**
     * La liste des formes à dessiner.
     */
    private List<Forme> formes;

    /**
     * La liste des formes sélectionnées.
     */
    private List<Forme> formesSelectionnees;

    /**
     * Indicateur qui est vrai si la liste des formes a changée.
     */
    private boolean nouvelEtat;

    /**
     * Crée le modèle.
     */
    public CanevasModele() {
        formes = new LinkedList<>();
        formesSelectionnees = new LinkedList<>();
        nouvelEtat = false;
    }

    /**
     * Retourne une copie de la liste des formes.
     *
     * @return Copie de la liste des formes.
     */
    public List<Forme> getFormes() {
        return List.copyOf(formes);
    }

    /**
     * Indique si l'état du modèle a changé.
     * L'état change lorsqu'une forme est ajoutée, modifiée ou supprimée.
     * <p>
     * L'état actuel devient normal après avoir informé du
     * changement d'état. Exécuter cette fonction deux fois retournera "false"
     * au deuxième coup, sauf si l'état change de nouveau.
     *
     * @return Vrai si l'état a changé
     */
    public boolean aChangeEtat() {
        boolean resultat = nouvelEtat;
        nouvelEtat = false;
        return resultat;
    }

    /**
     * Ajoute une forme dans la liste.
     *
     * @param forme La forme à ajouter
     */
    public void ajouteForme(Forme forme) {
        formes.add(forme);
        nouvelEtat = true;
    }

    /**
     * Retire les formes qui sont dans la liste {@link #formesSelectionnees}.
     */
    public void retireSelection() {
        formes.removeAll(formesSelectionnees);
        formesSelectionnees.clear();
        nouvelEtat = true;
    }

    /**
     * Vide la liste de formes.
     */
    public void retireTout() {
        formes.clear();
        formesSelectionnees.clear();
        nouvelEtat = true;
    }

    /**
     * Sélectionne une forme avec une coordonnée et la met dans la liste.
     * <p>
     * Une forme est sélectionnée si la coordonnée intersecte un segment ou si
     * elle est contenue à l'intérieur d'une figure pleine. La dernière forme
     * ajoutée sera testée en premier puisqu'elle est affichée sur le dessus
     * lorsque deux formes se superposent. Si aucune forme n'est sélectionnée,
     * toutes les formes se désélectionnent.
     *
     * @param point La coordonnée à tester
     * @see Forme#intersecte(Point)
     * @see FigurePlane#contiens(Point)
     */
    public void selectionne(Point point) {
        Forme selection;
        ArrayList<Forme> inverse = new ArrayList<>(formes);
        Collections.reverse(inverse);
        selection = inverse.stream()
                .filter(forme -> {
                    boolean selectionnable;
                    if ((forme instanceof FigurePlane) &&
                            (((FigurePlane) forme).estPlein())) {
                        selectionnable = ((FigurePlane) forme).contiens(point);
                    } else {
                        selectionnable = forme.intersecte(point);
                    }
                    return selectionnable;
                })
                .findFirst()
                .orElse(null);
        if (selection != null) {
            selection.selectionne();
            if (!formesSelectionnees.contains(selection)) {
                formesSelectionnees.add(selection);
            }
        } else {
            deselectionneTout();
        }
        nouvelEtat = true;
    }

    /**
     * Désélectionne les formes dans {@link #formesSelectionnees} et vide la
     * liste.
     */
    public void deselectionneTout() {
        formesSelectionnees.forEach(forme -> forme.deselectionne());
        formesSelectionnees.clear();
        nouvelEtat = true;
    }


}
