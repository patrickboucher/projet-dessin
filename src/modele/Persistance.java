package modele;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.Closeable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Persistance permet de lire ou d'écrire un objet dans un fichier.
 */
public class Persistance {

    /**
     * L'extension d'un fichier qui peut être lu ou écrit par le programme.
     */
    public final static String EXTENSION = "projetdessin";

    /**
     * La description de l'extension de fichier.
     */
    public final static String DESCRIPTION = "Projet dessin (.projetdessin)";

    /**
     * Le sélecteur de fichier utilisé par l'utilisateur.
     */
    private JFileChooser selecteurFichier;

    /**
     * Un message d'erreur lorsqu'il y a un problème avec un fichier.
     */
    private String message;

    /**
     * Crée un objet persistance.
     */
    public Persistance() {
        selecteurFichier = new JFileChooser();
        selecteurFichier.setFileFilter(
                new FileNameExtensionFilter(DESCRIPTION, EXTENSION));
    }

    /**
     * Retourne le message d'erreur.
     * Retourne une chaîne vide s'il n'y a pas eu d'erreur.
     *
     * @return Le message d'erreur
     * @see #aMessage()
     */
    public String getMessage() {
        return message != null ? message : "";
    }

    /**
     * Indique s'il y a eu un message après la dernière lecture ou écriture.
     *
     * @return "true" s'il y a un message d'erreur
     */
    public boolean aMessage() {
        return message != null && !message.isEmpty();
    }

    /**
     * Retourne le nom du fichier, ou null si non applicable.
     *
     * @return Le nom du fichier
     */
    public String getNomFichier() {
        String fichier = null;
        if (selecteurFichier.getSelectedFile() != null) {
            fichier = selecteurFichier.getSelectedFile().getName();
        }
        return fichier;
    }

    /**
     * Écris un objet dans un fichier.
     * L'utilisateur choisit le nom et l'emplacement du fichier à écrire.
     * Un message est défini si le fichier n'a pas pu être enregistré.
     *
     * @param objet L'objet à sauvegarder
     * @see #aMessage()
     */
    public void ecrire(Object objet) {
        int etat = selecteurFichier.showSaveDialog(null);
        message = null;
        if (etat == JFileChooser.APPROVE_OPTION) {
            ObjectOutputStream ecriture = null;
            String url = selecteurFichier.getSelectedFile().getPath();
            if (!extensionValide(url)) {
                url = url.concat("." + EXTENSION);
            }
            try {
                ecriture = new ObjectOutputStream(new FileOutputStream(url));
                ecriture.writeObject(objet);
                ecriture.flush();
            } catch (IOException exception) {
                message = "Une erreur s'est produite et " +
                        "le fichier n'a pu être enregistré.";
            } finally {
                ferme(ecriture);
            }
        }
    }

    /**
     * Lis et retourne un objet dans un fichier.
     * L'utilisateur sélectionne le fichier .projetdessin à lire.
     * Un message est défini si le fichier ne peut pas être lu.
     *
     * @return L'objet stocké dans le fichier
     * @see #aMessage()
     */
    public Object lire() {
        Object resultat = null;
        int etat = selecteurFichier.showOpenDialog(null);
        message = null;
        if (etat == JFileChooser.APPROVE_OPTION) {
            String url = selecteurFichier.getSelectedFile().getPath();
            if (extensionValide(url)) {
                ObjectInputStream lecture = null;
                try {
                    lecture = new ObjectInputStream(new FileInputStream(url));
                    resultat = lecture.readObject();
                } catch (IOException | ClassNotFoundException exception) {
                    message = "Le fichier " +
                            selecteurFichier.getSelectedFile().getName() +
                            " ne peut pas être lu.";
                } finally {
                    ferme(lecture);
                }
            } else {
                message = "L'extension du fichier " +
                        selecteurFichier.getSelectedFile().getName() +
                        " n'est pas reconnue.";
            }
        }
        return resultat;
    }

    /**
     * Vérifie si le nom ou l'adresse du fichier contient la bonne
     * {@link #EXTENSION extension}.
     *
     * @param fichier Le nom ou l'adresse du fichier
     * @return "true" si l'extension est valide
     */
    private boolean extensionValide(String fichier) {
        String extension = "";
        if (fichier != null && fichier.contains(".")) {
            extension = fichier.substring(fichier.lastIndexOf("."));
        }
        return extension.equals("." + EXTENSION);
    }

    /**
     * Ferme silencieusement un objet {@link Closeable} s'il n'est pas null.
     *
     * @param stream L'objet à fermer
     */
    private void ferme(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }

}
