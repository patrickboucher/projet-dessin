package modele.forme;

import java.awt.Color;
import java.awt.Point;

/**
 * Une {@link FigurePlane} qui possède les informations pour dessiner un carré.
 * Quatre points cartésiens sont générés pour permettre le traçage des segments.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class Carre extends Rectangle {

    /**
     * Crée le carré en lui assignant une position, la mesure d'un segment et
     * une couleur de fond par défaut.
     *
     * @param position La position de l'origine
     * @param cote     La mesure d'un segment
     */
    public Carre(Point position, int cote) {
        this(position, cote, COULEUR_PAR_DEFAUT);
    }

    /**
     * Crée le carré en lui assignant une position, la mesure d'un segment et
     * une couleur de fond.
     *
     * @param position La position de l'origine
     * @param cote     La mesure d'un segment
     * @param couleur  La couleur de fond
     */
    public Carre(Point position, int cote, Color couleur) {
        this(position.x, position.y, cote, couleur);
    }

    /**
     * Crée le carré en lui assignant une position, la mesure d'un segment et
     * une couleur de fond.
     *
     * @param positionX La position x de l'origine
     * @param positionY La position y de l'origine
     * @param cote      La mesure d'un segment
     * @param couleur   La couleur de fond
     */
    public Carre(int positionX, int positionY, int cote, Color couleur) {
        super(positionX, positionY, cote, cote, couleur);
    }

    /**
     * Retourne la mesure d'un segment.
     *
     * @return La mesure d'un segment
     */
    public int getCote() {
        return getLongueur();
    }

    /**
     * Assigne la mesure d'un segment.
     *
     * @param valeur La mesure d'un segment
     */
    public void setCote(int valeur) {
        setDimensions(valeur, valeur);
    }

    /**
     * Assigne la longueur du carré. La hauteur est automatiquement ajustée.
     *
     * @param valeur La longueur du carré
     */
    @Override
    public void setLongueur(int valeur) {
        setCote(valeur);
    }

    /**
     * Assigne la hauteur du carré. La longueur est automatiquement ajustées.
     *
     * @param valeur La hauteur du carré
     */
    @Override
    public void setHauteur(int valeur) {
        setCote(valeur);
    }
}
