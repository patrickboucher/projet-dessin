package modele.forme;

import java.awt.Color;
import java.awt.Point;

/**
 * Une {@link FigurePlane} qui possède les informations pour dessiner un cercle.
 * Quatre points cartésiens sont générés pour permettre le traçage des arcs à
 * l'intérieur d'un carré de référence. La longueur et la hauteur du carré
 * représentent le diamètre du cercle.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class Cercle extends Ellipse {

    /**
     * Crée le cercle en assignant la position, le rayon et la couleur de fond
     * par défaut.
     *
     * @param position La position de l'origine
     * @param rayon    Le rayon du cercle
     */
    public Cercle(Point position, double rayon) {
        this(position, rayon, COULEUR_PAR_DEFAUT);
    }

    /**
     * Crée le cercle en assignant la position, le rayon et la couleur de fond.
     *
     * @param position La position de l'origine
     * @param rayon    Le rayon du cercle
     * @param couleur  La couleur de fond
     */
    public Cercle(Point position, double rayon, Color couleur) {
        this(position.x, position.y, rayon, couleur);
    }

    /**
     * Crée le cercle en assignant la position, le rayon et la couleur de fond
     * par défaut.
     *
     * @param positionX La position x de l'origine
     * @param positionY La position y de l'origine
     * @param rayon     Le rayon du cercle
     */
    public Cercle(int positionX, int positionY, double rayon) {
        this(positionX, positionY, rayon, COULEUR_PAR_DEFAUT);
    }

    /**
     * Crée le cercle en assignant la position, le rayon et la couleur de fond.
     *
     * @param positionX La position x de l'origine
     * @param positionY La position y de l'origine
     * @param rayon     Le rayon du cercle
     * @param couleur   La couleur de fond
     */
    public Cercle(int positionX, int positionY, double rayon, Color couleur) {
        super(positionX, positionY, 0, 0, couleur);
        setRayon(rayon);
    }

    /**
     * Retourne le rayon du cercle.
     *
     * @return Le rayon du cercle
     */
    public double getRayon() {
        return getLongueur() / 2.0;
    }

    /**
     * Assigne le rayon du cercle.
     *
     * @param valeur Le rayon du cercle
     */
    public void setRayon(double valeur) {
        int diametre = (int) Math.round(valeur * 2);
        setLongueur(diametre);
    }

    /**
     * Assigne la longueur du carré de référence.
     *
     * @param diametre Le diamètre du cercle
     */
    @Override
    public void setLongueur(int diametre) {
        super.setLongueur(diametre);
        super.setHauteur(diametre);
    }

    /**
     * Assigne la hauteur du carré de référence.
     *
     * @param diametre Le diamètre du cercle
     */
    @Override
    public void setHauteur(int diametre) {
        setLongueur(diametre);
    }
}
