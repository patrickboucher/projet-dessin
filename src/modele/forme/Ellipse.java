package modele.forme;

import java.awt.Color;
import java.awt.Point;

/**
 * Une {@link FigurePlane} qui possède les informations pour dessiner un
 * ellipse. Quatre points cartésiens sont générés pour permettre le traçage
 * des arcs à l'intérieur d'un rectangle de référence.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class Ellipse extends FigurePlane {

    /**
     * La longueur du rectangle de référence.
     */
    private int longueur;

    /**
     * La hauteur du rectangle de référence.
     */
    private int hauteur;

    /**
     * Crée l'ellipse en lui assignant une position, une longueur, une hauteur
     * et une couleur de fond par défaut.
     *
     * @param position La position de l'origine
     * @param longueur La longueur du rectangle de référence
     * @param hauteur  La hauteur du rectangle de référence
     */
    public Ellipse(Point position, int longueur, int hauteur) {
        this(position, longueur, hauteur, COULEUR_PAR_DEFAUT);
    }

    /**
     * Crée l'ellipse en lui assignant une position, une longueur, une hauteur
     * et une couleur de fond.
     *
     * @param position La position de l'origine
     * @param longueur La longueur du rectangle de référence
     * @param hauteur  La hauteur du rectangle de référence
     * @param couleur  La couleur de fond
     */
    public Ellipse(Point position, int longueur, int hauteur, Color couleur) {
        this(position.x, position.y, longueur, hauteur, couleur);
    }

    /**
     * Crée l'ellipse en lui assignant une position, une longueur, une hauteur
     * et une couleur de fond par défaut.
     *
     * @param positionX La position x de l'origine
     * @param positionY La position y de l'origine
     * @param longueur  La longueur du rectangle de référence
     * @param hauteur   La hauteur du rectangle de référence
     */
    public Ellipse(int positionX, int positionY, int longueur, int hauteur) {
        this(positionX, positionY, longueur, hauteur, COULEUR_PAR_DEFAUT);
    }

    /**
     * Crée l'ellipse en lui assignant une position, une longueur, une hauteur
     * et une couleur de fond.
     *
     * @param positionX La position x de l'origine
     * @param positionY La position y de l'origine
     * @param longueur  La longueur du rectangle de référence
     * @param hauteur   La hauteur du rectangle de référence
     * @param couleur   La couleur de fond
     */
    public Ellipse(int positionX, int positionY,
                   int longueur, int hauteur, Color couleur) {
        super(positionX, positionY, couleur);
        this.longueur = longueur;
        this.hauteur = hauteur;
    }

    /**
     * Retourne la longueur du rectangle de référence.
     *
     * @return La longueur du rectangle de référence
     */
    public int getLongueur() {
        return longueur;
    }

    /**
     * Assigne la longueur du rectangle de référence.
     *
     * @param valeur La longueur du rectangle de référence
     */
    public void setLongueur(int valeur) {
        longueur = valeur;
    }

    /**
     * Retourne la hauteur du rectangle de référence.
     *
     * @return La hauteur du rectangle de référence
     */
    public int getHauteur() {
        return hauteur;
    }

    /**
     * Assigne la hauteur du rectangle de référence.
     *
     * @param valeur La hauteur du rectangle de référence
     */
    public void setHauteur(int valeur) {
        hauteur = valeur;
    }

    /**
     * Indique si la coordonnée intersecte la courbe.
     *
     * @param point La coordonée
     * @return "true" si la coordonnée intersecte la courbe
     * @throws NullPointerException Si l'argument est null
     */
    @Override
    public boolean intersecte(Point point) {
        int distance = (int) Math.round(calculeDistanceFoyers(point));
        int grandAxe = Math.max(hauteur, longueur);
        return (distance >= grandAxe - (TOLERANCE_SELECTION + 1)) &&
                (distance <= grandAxe + (TOLERANCE_SELECTION + 1));
    }

    /**
     * {@inheritDoc}
     *
     * @throws NullPointerException Si l'argument est null
     */
    @Override
    public boolean contiens(Point point)  {
        return calculeDistanceFoyers(point) <= Math.max(hauteur, longueur);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void metsAJourListePoints() {
        Point point2 = new Point(position.x + longueur, position.y + hauteur);
        points.clear();
        points.add(position);
        points.add(point2);
    }

    /**
     * Calcule la distance des foyers de l'ellipse par rapport à une coordonnée.
     *
     * @param point La coordonnée
     * @return La distance totale des deux foyers par rapport à la coordonnée
     * @throws NullPointerException Si l'argument est null
     */
    private double calculeDistanceFoyers(Point point)
            throws NullPointerException {
        if (point == null) {
            throw new NullPointerException("L'argument ne peut pas être null");
        }
        Point foyer1 = new Point();
        Point foyer2 = new Point();
        double a = longueur / 2.0;
        double b = hauteur / 2.0;
        double h = position.x + a;
        double k = position.y + b;
        if (a < b) {
            double c = Math.sqrt(Math.pow(b, 2) - Math.pow(a, 2));
            foyer1.setLocation(h, k + c);
            foyer2.setLocation(h, k - c);
        } else {
            double c = Math.sqrt(Math.pow(a, 2) - Math.pow(b, 2));
            foyer1.setLocation(h + c, k);
            foyer2.setLocation(h - c, k);
        }
        return calculeDistancePoints(point, foyer1) +
                calculeDistancePoints(point, foyer2);
    }

}
