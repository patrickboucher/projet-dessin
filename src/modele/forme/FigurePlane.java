package modele.forme;

import java.awt.Color;
import java.awt.Point;

/**
 * Une {@link Forme} en deux dimensions qui peut posséder une bordure et une
 * couleur de fond.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public abstract class FigurePlane extends Forme {

    /**
     * Indique si la couleur de fond doit être dessiné.
     */
    protected boolean plein;

    /**
     * Un indicateur pour savoir si la forme contient une bordure.
     */
    protected boolean bordure;

    /**
     * L'épaisseur de la bordure.
     */
    protected int epaisseurBodure;

    /**
     * La couleur de la bordure.
     */
    protected Color couleurBordure;

    /**
     * Crée la figure en lui assignant une position et une couleur de fond par
     * défaut.
     *
     * @param position La position de l'origine
     */
    public FigurePlane(Point position) {
        this(position.x, position.y, COULEUR_PAR_DEFAUT);
    }

    /**
     * Crée la figure en lui assignant une position et une couleur de fond.
     *
     * @param position La position de l'origine
     * @param couleur  La couleur de fond
     */
    public FigurePlane(Point position, Color couleur) {
        this(position.x, position.y, couleur);
    }

    /**
     * Crée la figure en lui assignant une position et une couleur de fond.
     *
     * @param positionX La position x de l'origine
     * @param positionY La position y de l'origine
     * @param couleur   La couleur de fond
     */
    public FigurePlane(int positionX, int positionY, Color couleur) {
        super(positionX, positionY, couleur);
        bordure = false;
        epaisseurBodure = 1;
    }

    /**
     * Retourne l'épaisseur de la bordure.
     *
     * @return L'épaisseur de la bordure
     */
    public int getEpaisseurBodure() {
        return epaisseurBodure;
    }

    /**
     * Assigne l'épaisseur de la bordure. L'épaisseur ne peut être négative.
     *
     * @param valeur L'épaisseur de la bordure
     */
    public void setEpaisseurBodure(int valeur) {
        epaisseurBodure = Math.max(valeur, 0);
    }

    /**
     * Indique si la couleur de fond doit être dessinée.
     *
     * @return "true" si la couleur de fond doit être dessinée
     */
    public boolean estPlein() {
        return plein;
    }

    /**
     * Défini si la couleur de fond doit être dessinée.
     *
     * @param valeur "true" si la couleur doit être dessinée
     */
    public void setPlein(boolean valeur) {
        plein = valeur;
    }

    /**
     * Indique si la figure a une bordure.
     *
     * @return "true" si la figure a une bordure
     */
    public boolean aBordure() {
        return bordure;
    }

    /**
     * Défini si la figure doit posséder une bordure ou non.
     *
     * @param valeur "true" si la figure doit posséder une bordure
     */
    public void setBordure(boolean valeur) {
        bordure = valeur;
    }

    public Color getCouleurBordure() {
        return couleurBordure;
    }

    public void setCouleurBordure(Color couleur) {
        if (couleur == null) {
            throw new NullPointerException("La couleur doit être définie.");
        }
        couleurBordure = couleur;
    }

    /**
     * Définis que la figure doit posséder une bordure.
     */
    public void activeBordure() {
        setBordure(true);
    }

    /**
     * Définis que la figure ne doit pas posséder de bordure.
     */
    public void desactiveBordure() {
        setBordure(false);
    }

    /**
     * Indique si une coordonnée est à l'intérieur de la figure.
     *
     * @param point La coordonnée
     * @return "true" si la coordonnée est contenue par la figure
     */
    public abstract boolean contiens(Point point);

}
