package modele.forme;

import java.awt.Color;
import java.awt.Point;
import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Objet qui contient les informations servant à générer des points cartésiens
 * pour ainsi être dessiné dans un {@link vue.CanevasVue canevas}.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 * @see java.awt.Graphics2D
 */
public abstract class Forme implements Serializable {

    /**
     * La couleur par défaut si aucune couleur n'est spécifiée.
     */
    protected final static Color COULEUR_PAR_DEFAUT = Color.BLACK;

    /**
     * La tolérance de distance lorsqu'on teste si un point intersecte la forme.
     *
     * @see #intersecte(Point)
     */
    protected final static int TOLERANCE_SELECTION = 2;

    /**
     * La liste des points cartésiens qui servent au traçage des lignes.
     */
    protected List<Point> points;

    /**
     * La position de l'origine de la forme.
     */
    protected Point position;

    /**
     * La couleur de la forme.
     */
    protected Color couleur;

    /**
     * Indique si la forme est sélectionnée.
     */
    protected boolean selection;

    /**
     * Crée la forme en lui assignant une position et une couleur par défaut.
     *
     * @param position La position du point d'origine dans le canevas.
     */
    public Forme(Point position) {
        this(position, COULEUR_PAR_DEFAUT);
    }

    /**
     * Crée la forme en lui assignant une position et une couleur.
     *
     * @param position La position du point d'origine dans le canevas.
     * @param couleur  La couleur
     */
    public Forme(Point position, Color couleur) {
        this(position.x, position.y, couleur);
    }

    /**
     * Crée la forme en lui assignant une position et une couleur.
     *
     * @param positionX La position x du point d'origine dans le canevas.
     * @param positionY La position y du point d'origine dans le canevas.
     * @param couleur   La couleur
     */
    public Forme(int positionX, int positionY, Color couleur) {
        position = new Point(positionX, positionY);
        this.couleur = couleur;
        points = new LinkedList<>();
    }

    /**
     * Retourne une copie de la liste de points cartésiens.
     * <p>
     * La liste est d'abord recréée pour s'assurer qu'elle est à jour avec les
     * propriétés de la forme.
     *
     * @return Copie de la liste de points cartésiens
     */
    public Collection<Point> getPoints() {
        metsAJourListePoints();
        return List.copyOf(points);
    }

    /**
     * Retourne la couleur de la forme.
     *
     * @return La couleur de la forme
     */
    public Color getCouleur() {
        return couleur;
    }

    /**
     * Assigne la couleur de la forme.
     *
     * @param couleur La couleur
     */
    public void setCouleur(Color couleur) {
        this.couleur = couleur;
    }

    /**
     * Retourne la position de l'origine.
     *
     * @return La position de l'origine
     */
    public Point getPosition() {
        return new Point(position);
    }

    /**
     * Assigne la position de l'origine.
     *
     * @param point La position de l'origine
     * @throws NullPointerException Si l'argument est null
     */
    public void setPosition(Point point) {
        position.setLocation(point);
    }

    /**
     * Assigne la position de l'origine.
     *
     * @param x La position x de l'origine
     * @param y La position y de l'origine
     */
    public void setPosition(int x, int y) {
        position.setLocation(x, y);
    }

    /**
     * Assigne l'état de sélection à la forme.
     *
     * @param valeur "true" pour l'état "sélectionnée"
     */
    public void setSelection(boolean valeur) {
        selection = valeur;
    }

    /**
     * Assigne l'état "sélectionnée" à la forme.
     * {@link #estSelectionne()} retournera "true".
     */
    public void selectionne() {
        setSelection(true);
    }

    /**
     * Assigne l'état "non sélectionnée" à la forme.
     * {@link #estSelectionne()} retournera "false".
     */
    public void deselectionne() {
        setSelection(false);
    }

    /**
     * Retourne l'état de sélection de la forme.
     *
     * @return "true" si la forme est sélectionnée
     */
    public boolean estSelectionne() {
        return selection;
    }

    /**
     * Vérifie si une coordonnée fait partie d'un segment ou de la courbe.
     *
     * @param point La coordonée
     * @return "true" si la coordonnée intersecte un segment ou la courbe
     */
    public abstract boolean intersecte(Point point);

    /**
     * Mets à jour la liste des points cartésiens en fonctions des propriétés de
     * la forme.
     */
    protected abstract void metsAJourListePoints();

    /**
     * Calcule la distance entre deux coordonnées.
     *
     * @param point1 La première coordonnée
     * @param point2 La deuxième coordonnée
     * @return La distance entre deux coordonnées
     * @throws NullPointerException Si l'un des argument est null
     */
    protected double calculeDistancePoints(Point point1, Point point2) {
        return calculeDistancePoints(point1.x, point1.y, point2.x, point2.y);
    }

    /**
     * Calcule la distance entre deux coordonnées.
     *
     * @param x1 Variable x de la première coordonnée
     * @param y1 Variable y de la première coordonnée
     * @param x2 Variable x de la deuxième coordonnée
     * @param y2 Variable y de la deuxième coordonnée
     * @return La distance entre deux coordonnées
     */
    protected double calculeDistancePoints(int x1, int y1, int x2, int y2) {
        return Math.sqrt(Math.pow(y2 - y1, 2) + Math.pow(x2 - x1, 2));
    }

}
