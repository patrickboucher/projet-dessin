package modele.forme;

import java.awt.Color;
import java.awt.Point;

/**
 * Une droite reliant deux points cartésiens.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class Ligne extends Forme {

    /**
     * Le point à relier avec l'origine.
     */
    private Point point2;

    /**
     * Épaisseur de la ligne en pixel.
     */
    private int epaisseur;

    /**
     * Crée la ligne en lui assignant les positions de deux points et une
     * couleur par défaut.
     *
     * @param point1 La position du premier point
     * @param point2 La position du deuxième point
     */
    public Ligne(Point point1, Point point2) {
        this(point1, point2, COULEUR_PAR_DEFAUT);
    }

    /**
     * Crée la ligne en lui assignant les positions de deux points et une
     * couleur.
     *
     * @param point1  La position du premier point
     * @param point2  La position du deuxième point
     * @param couleur La couleur de la ligne
     */
    public Ligne(Point point1, Point point2, Color couleur) {
        this(point1.x, point1.y, point2.x, point2.y, couleur);
    }

    /**
     * Crée la ligne en lui assignant les positions de deux points et une
     * couleur par défaut.
     *
     * @param point1X La position x du premier point
     * @param point1Y La position y du premier point
     * @param point2X La position x du deuxième point
     * @param point2Y La position y du deuxième point
     */
    public Ligne(int point1X, int point1Y, int point2X, int point2Y) {
        this(point1X, point1Y, point2X, point2Y, COULEUR_PAR_DEFAUT);
    }

    /**
     * Crée la ligne en lui assignant les positions de deux points et une
     * couleur.
     *
     * @param point1X La position x du premier point
     * @param point1Y La position y du premier point
     * @param point2X La position x du deuxième point
     * @param point2Y La position y du deuxième point
     * @param couleur La couleur de la ligne
     */
    public Ligne(int point1X, int point1Y,
                 int point2X, int point2Y, Color couleur) {
        super(point1X, point1Y, couleur);
        point2 = new Point(point2X, point2Y);
        epaisseur = 1;
    }

    /**
     * Retourne la position du premier point.
     *
     * @return La position du premier point
     */
    public Point getPoint1() {
        return getPosition();
    }

    /**
     * Assigne la position du premier point.
     *
     * @param valeur La position du premier point
     */
    public void setPoint1(Point valeur) {
        setPosition(valeur);
    }

    /**
     * Assigne la position du premier point.
     *
     * @param x La position x du premier point
     * @param y La position y du premier point
     */
    public void setPoint1(int x, int y) {
        setPosition(x, y);
    }

    /**
     * Retourne la position du deuxième point.
     *
     * @return La position du deuxième point
     */
    public Point getPoint2() {
        return new Point(point2);
    }

    /**
     * Assigne la position du deuxième point.
     *
     * @param point La position du deuxième point
     */
    public void setPoint2(Point point) {
        point2.setLocation(point);
    }

    /**
     * Assigne la position du deuxième point.
     *
     * @param x La position x du deuxième point
     * @param y La position y du deuxième point
     */
    public void setPoint2(int x, int y) {
        point2.setLocation(x, y);
    }

    /**
     * Retourne l'épaisseur en nombre de pixels.
     *
     * @return L'épaisseur
     */
    public int getEpaisseur() {
        return epaisseur;
    }

    /**
     * Assigne l'épaisseur.
     * L'épaisseur est un nombre égal ou supérieur à 1.
     *
     * @param valeur Épaisseur en pixels
     * @throws IllegalArgumentException epaisseur doit être supérieur à 0
     */
    public void setEpaisseur(int valeur) throws IllegalArgumentException {
        if (valeur < 1) {
            throw new IllegalArgumentException("Doit être supérieur à 0");
        }
        epaisseur = valeur;
    }

    /**
     * Retourne la longueur du segment.
     *
     * @return La longueur du segment
     */
    public double getLongueur() {
        return calculeDistancePoints(position.x, position.y,
                point2.x, point2.y);
    }

    /**
     * Indique si une coordonnée touche le segment en calculant sa distance.
     *
     * @param point La coordonée
     * @return "true" si la coordonnée touche le segment
     * @throws NullPointerException Si l'argument est null
     * @see #TOLERANCE_SELECTION
     */
    @Override
    public boolean intersecte(Point point) {
        boolean touche = false;
        int x0 = point.x;
        int y0 = point.y;
        int x1 = position.x;
        int y1 = position.y;
        int x2 = point2.x;
        int y2 = point2.y;
        double distancePoint1 = calculeDistancePoints(x0, y0, x1, y1);
        double distancePoint2 = calculeDistancePoints(x0, y0, x2, y2);
        if ((distancePoint1 <= getLongueur()) &&
                (distancePoint2 <= getLongueur())) {
            // https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
            double distance = Math.abs(
                    ((y2 - y1) * x0) - ((x2 - x1) * y0) + (x2 * y1) - (y2 * x1))
                    / calculeDistancePoints(x1, y1, x2, y2);
            touche = distance <= Math.max(epaisseur / 2, TOLERANCE_SELECTION);
        }
        return touche;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void metsAJourListePoints() {
        points.clear();
        points.add(getPoint1());
        points.add(getPoint2());
    }

}
