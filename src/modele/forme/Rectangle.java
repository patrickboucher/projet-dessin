package modele.forme;

import java.awt.Color;
import java.awt.Point;
import java.util.List;

/**
 * Une {@link FigurePlane} qui possède les informations pour dessiner un
 * rectangle. Quatre points cartésiens sont générés pour permettre le traçage
 * des segments.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class Rectangle extends FigurePlane {

    /**
     * La longueur du rectangle.
     */
    private int longueur;

    /**
     * La hauteur du rectangle.
     */
    private int hauteur;

    /**
     * Crée le rectangle en lui assignant une position, une longueur, une
     * hauteur et une couleur de fond par défaut.
     *
     * @param position La position de l'origine
     * @param longueur La longueur du rectangle
     * @param hauteur  La hauteur du rectangle
     */
    public Rectangle(Point position, int longueur, int hauteur) {
        this(position, longueur, hauteur, COULEUR_PAR_DEFAUT);
    }

    /**
     * Crée le rectangle en lui assignant une position, une longueur, une
     * hauteur et une couleur de fond par défaut.
     *
     * @param positionX La position x de l'origine
     * @param positionY La position y de l'origine
     * @param longueur  La longueur du rectangle
     * @param hauteur   La hauteur du rectangle
     */
    public Rectangle(int positionX, int positionY, int longueur, int hauteur) {
        this(positionX, positionY, longueur, hauteur, COULEUR_PAR_DEFAUT);
    }

    /**
     * Crée le rectangle en lui assignant une position, une longueur, une
     * hauteur et une couleur de fond.
     *
     * @param position La position de l'origine
     * @param longueur La longueur du rectangle
     * @param hauteur  La hauteur du rectangle
     * @param couleur  La couleur de fond
     */
    public Rectangle(Point position, int longueur, int hauteur, Color couleur) {
        this(position.x, position.y, longueur, hauteur, couleur);
    }

    /**
     * Crée le rectangle en lui assignant une position, une longueur, une
     * hauteur et une couleur de fond.
     *
     * @param positionX La position x de l'origine
     * @param positionY La position y de l'origine
     * @param longueur  La longueur du rectangle
     * @param hauteur   La hauteur du rectangle
     * @param couleur   La couleur de fond
     */
    public Rectangle(int positionX, int positionY,
                     int longueur, int hauteur, Color couleur) {
        super(positionX, positionY, couleur);
        this.longueur = longueur;
        this.hauteur = hauteur;
    }

    /**
     * Retourne la longueur du rectangle.
     *
     * @return La longueur du rectangle
     */
    public int getLongueur() {
        return longueur;
    }

    /**
     * Assigne la longueur du rectangle.
     *
     * @param valeur La longueur du rectangle
     */
    public void setLongueur(int valeur) {
        longueur = valeur;
    }

    /**
     * Retourne la hauteur du rectangle.
     *
     * @return La hauteur du rectangle
     */
    public int getHauteur() {
        return hauteur;
    }

    /**
     * Assigne la hauteur du rectangle.
     *
     * @param valeur La hauteur du rectangle
     */
    public void setHauteur(int valeur) {
        hauteur = valeur;
    }

    /**
     * Assigne la longueur et la hauteur du rectangle.
     *
     * @param longueur La longueur du rectangle
     * @param hauteur  La hauteur du rectangle
     */
    public void setDimensions(int longueur, int hauteur) {
        setLongueur(longueur);
        setHauteur(hauteur);
    }

    /**
     * Indique si la coordonnée intersecte un des segments du rectangle.
     *
     * @param point La coordonée
     * @return "true" si la coordonnée intersecte le rectangle
     * @throws NullPointerException Si l'argument est null
     */
    @Override
    public boolean intersecte(Point point) {
        Point point1 = new Point(position.x, position.y);
        Point point2 = new Point(position.x, position.y + hauteur);
        Point point3 = new Point(position.x + longueur, position.y + hauteur);
        Point point4 = new Point(position.x + longueur, position.y);
        List<Ligne> lignes = List.of(
                new Ligne(point1, point2),
                new Ligne(point2, point3),
                new Ligne(point3, point4),
                new Ligne(point4, point1));
        return lignes.stream().anyMatch(ligne -> ligne.intersecte(point));
    }

    /**
     * {@inheritDoc}
     *
     * @throws NullPointerException Si l'argument est null
     */
    @Override
    public boolean contiens(Point point) {
        return (point.x >= position.x) && (point.x <= position.x + longueur) &&
                (point.y >= position.y) && (point.y <= position.y + hauteur);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void metsAJourListePoints() {
        int x1 = position.x;
        int y1 = position.y;
        int x2 = position.x + longueur;
        int y2 = position.y + hauteur;
        points.clear();
        points.add(new Point(x1, y1));
        points.add(new Point(x2, y1));
        points.add(new Point(x2, y2));
        points.add(new Point(x1, y2));
    }
}
