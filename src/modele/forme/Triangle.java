package modele.forme;

import java.awt.Point;
import java.util.List;

/**
 * Une {@link FigurePlane} qui possède les informations pour dessiner un
 * triangle. Trois points cartésiens sont générés pour permettre le traçage des
 * segments. Le premier point s'agit de la position de la figure.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class Triangle extends FigurePlane {

    /**
     * Deuxième point du triangle.
     */
    private Point point2;

    /**
     * Troisième point du triangle.
     */
    private Point point3;

    /**
     * Construis le triangle en assignant les trois points cartésiens.
     *
     * @param point1 Le premier point
     * @param point2 Le deuxième point
     * @param point3 Le troisième point
     * @throws NullPointerException Si l'un des argument est null
     */
    public Triangle(Point point1, Point point2, Point point3) {
        this(point1.x, point1.y, point2.x, point2.y, point3.x, point3.y);
    }

    /**
     * Construis le triangle en assignant les trois points cartésiens.
     *
     * @param x1 La valeur x du premier point
     * @param y1 La valeur y du premier point
     * @param x2 La valeur x du deuxième point
     * @param y2 La valeur y du deuxième point
     * @param x3 La valeur x du troisième point
     * @param y3 La valeur y du troisième point
     */
    public Triangle(int x1, int y1, int x2, int y2, int x3, int y3) {
        super(x1, y1, COULEUR_PAR_DEFAUT);
        setPoint2(x2, y2);
        setPoint3(x3, y3);
    }

    /**
     * Retourne le premier point.
     *
     * @return Le premier point
     */
    public Point getPoint1() {
        return getPosition();
    }

    /**
     * Assigne le premier point.
     *
     * @param point La valeur à assigner
     * @throws NullPointerException Si l'argument est null
     */
    public void setPoint1(Point point) {
        setPosition(point);
    }

    /**
     * Assigne le premier point.
     *
     * @param x La valeur x du point à assigner
     * @param y La valeur y du point à assigner
     */
    public void setPoint1(int x, int y) {
        setPoint1(new Point(x, y));
    }

    /**
     * Retourne le deuxième point.
     *
     * @return Le deuxième point
     */
    public Point getPoint2() {
        return point2;
    }

    /**
     * Assigne le deuxième point.
     *
     * @param point La valeur à assigner
     * @throws NullPointerException Si l'argument est null
     */
    public void setPoint2(Point point) throws NullPointerException {
        if (point == null) {
            throw new NullPointerException();
        }
        point2 = point;
    }

    /**
     * Assigne le deuxième point.
     *
     * @param x La valeur x du point à assigner
     * @param y La valeur y du point à assigner
     */
    public void setPoint2(int x, int y) {
        setPoint2(new Point(x, y));
    }

    /**
     * Retourne le troisième point.
     *
     * @return Le troisième point
     */
    public Point getPoint3() {
        return point3;
    }

    /**
     * Assigne le troisième point.
     *
     * @param point La valeur à assigner
     * @throws NullPointerException Si l'argument est null
     */
    public void setPoint3(Point point) throws NullPointerException {
        if (point == null) {
            throw new NullPointerException();
        }
        point3 = point;
    }

    /**
     * Assigne le troisième point.
     *
     * @param x La valeur x du point à assigner
     * @param y La valeur y du point à assigner
     */
    public void setPoint3(int x, int y) {
        setPoint3(new Point(x, y));
    }

    /**
     * Indique si la coordonnée intersecte un des segments du triangle.
     *
     * @param point La coordonée
     * @return "true" si la coordonnée touche un des segments
     * @throws NullPointerException Si l'argument est null
     */
    @Override
    public boolean intersecte(Point point) {
        List<Ligne> lignes = List.of(
                new Ligne(position, point2),
                new Ligne(point2, point3),
                new Ligne(point3, position));
        return lignes.stream().anyMatch(ligne -> ligne.intersecte(point));
    }

    /**
     * {@inheritDoc}
     *
     * @throws NullPointerException Si l'argument est null.
     */
    @Override
    public boolean contiens(Point point) {
        // Source :
        // github.com/SebLague/Gamedev-Maths/blob/master/PointInTriangle.cs
        double s1 = point3.y - position.y;
        double s2 = point3.x - position.x;
        double s3 = point2.y - position.y;
        double s4 = point.y - position.y;
        double w1 = (position.x * s1 + s4 * s2 - point.x * s1)
                / (s3 * s2 - (point2.x - position.x) * s1);
        double w2 = (s4 - w1 * s3) / s1;
        return (w1 >= 0) && (w2 >= 0) && ((w1 + w2) <= 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void metsAJourListePoints() {
        points.clear();
        points.add(position);
        points.add(point2);
        points.add(point3);
    }
}
