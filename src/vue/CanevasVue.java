package vue;

import modele.forme.Ellipse;
import modele.forme.FigurePlane;
import modele.forme.Forme;
import modele.forme.Ligne;
import modele.forme.Rectangle;
import modele.forme.Triangle;
import modele.CanevasModele;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.util.Collection;

/**
 * Canevas dans lequel les formes sont dessinées. Un seul canevas peut être créé
 * dans le programme.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class CanevasVue extends PanneauObservable {

    /**
     * Le modèle qui contient les informations pour dessiner les formes.
     */
    private CanevasModele modele;

    /**
     * Crée le canevas.
     */
    public CanevasVue(CanevasModele modele) {
        this.modele = modele;
        setLayout(null);
        setBounds(200, 0, 600, 600);
    }

    /**
     * Dessine toutes les formes stockées dans le {@link CanevasModele modèle}.
     * <p>
     * {@inheritDoc}
     *
     * @param graphics Le contexte graphique dans lequel on peut dessiner
     * @throws UnsupportedOperationException Si la gestion d'une classe
     *                                       {@link Forme} n'a pas été
     *                                       implémenté dans cette méthode.
     */
    @Override
    public void paint(Graphics graphics) throws UnsupportedOperationException {
        super.paint(graphics);
        Graphics2D graphiques = (Graphics2D) graphics;
        Collection<Forme> formes = modele.getFormes();
        for (Forme forme : formes) {
            if (forme instanceof Ligne) {
                dessineLigne(graphiques, (Ligne) forme);
            } else if (forme instanceof Ellipse) {
                dessineEllipse(graphiques, (Ellipse) forme);
            } else if (forme instanceof Rectangle) {
                dessinePolygone(graphiques, (FigurePlane) forme);
            } else if (forme instanceof Triangle) {
                dessinePolygone(graphiques, (FigurePlane) forme);
            } else {
                throw new UnsupportedOperationException(forme.getClass() +
                        " n'est pas supporté");
            }
        }
    }

    /**
     * Dessine une {@link Ligne} dans le contexte graphique de cette vue.
     *
     * @param graphiques Le contexte graphique dans lequel on peut dessiner
     * @param ligne      La {@code Ligne} à dessiner
     */
    private void dessineLigne(Graphics2D graphiques, Ligne ligne) {
        Color couleur;
        definisLigne(graphiques, ligne.getEpaisseur(), ligne.estSelectionne());
        if (ligne.estSelectionne()) {
            couleur = Color.PINK;
        } else {
            couleur = ligne.getCouleur();
        }
        graphiques.setColor(couleur);
        graphiques.drawLine(ligne.getPoint1().x, ligne.getPoint1().y,
                ligne.getPoint2().x, ligne.getPoint2().y);
    }

    /**
     * Dessine un {@link Ellipse} dans le contexte graphique de cette vue.
     *
     * @param graphiques Le contexte graphique dans lequel on peut dessiner
     * @param ellipse    L'{@code Ellipse} à dessiner
     */
    private void dessineEllipse(Graphics2D graphiques, Ellipse ellipse) {
        Ellipse2D.Double ellipse2D = new Ellipse2D.Double();
        ellipse2D.x = ellipse.getPosition().x;
        ellipse2D.y = ellipse.getPosition().y;
        ellipse2D.width = ellipse.getLongueur();
        ellipse2D.height = ellipse.getHauteur();
        dessineFigure(graphiques, ellipse2D, ellipse);
    }

    /**
     * Dessine un polygone dans le contexte graphique de cette vue.
     *
     * @param graphiques Le contexte graphique dans lequel on peut dessiner
     * @param forme      La {@link modele.forme.FigurePlane} à dessiner
     */
    private void dessinePolygone(Graphics2D graphiques, FigurePlane forme) {
        Collection<Point> points = forme.getPoints();
        Polygon polygon = new Polygon();
        if (points.size() >= 3) {
            points.forEach(point -> polygon.addPoint(point.x, point.y));
            dessineFigure(graphiques, polygon, forme);
        }
    }

    /**
     * Dessine une {@link FigurePlane} dans le contexte graphique de cette vue.
     *
     * @param graphiques Le contexte graphique dans lequel on peut dessiner
     * @param dessinable La {@link Shape} qui peut être tracée dans le contexte
     * @param figure     Le modèle qui a servis à construire l'objet dessinable
     */
    private void dessineFigure(Graphics2D graphiques,
                               Shape dessinable, FigurePlane figure) {
        definisLigne(graphiques,
                figure.getEpaisseurBodure(), figure.estSelectionne());
        if (figure.estSelectionne()) {
            graphiques.setColor(Color.PINK);
            if (figure.estPlein()) {
                graphiques.fill(dessinable);
            } else {
                graphiques.draw(dessinable);
            }
        } else {
            if (figure.estPlein()) {
                graphiques.setColor(figure.getCouleur());
                graphiques.fill(dessinable);
            }
            if (figure.aBordure()) {
                graphiques.setColor(figure.getCouleurBordure());
                graphiques.draw(dessinable);
            }
        }
    }

    /**
     * Définis l'apparence des lignes à tracer dans le contexte graphique.
     *
     * @param graphiques Le contexte graphique dans lequel on peut dessiner
     * @param epaisseur  Épaisseur des lignes de la forme
     * @param pointillee Si la ligne doit être pointillée
     */
    private void definisLigne(Graphics2D graphiques,
                              int epaisseur, boolean pointillee) {
        BasicStroke ligne;
        if (pointillee) {
            float[] longueurPoints = {2, 2};
            ligne = new BasicStroke(epaisseur + 2, BasicStroke.CAP_BUTT,
                    BasicStroke.JOIN_MITER, 1, longueurPoints, 2);
        } else {
            ligne = new BasicStroke(epaisseur);
        }
        graphiques.setStroke(ligne);
    }

}
