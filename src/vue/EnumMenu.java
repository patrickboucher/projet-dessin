package vue;

/**
 * Nom des sous-menus du {@link MenuContextuel}.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public enum EnumMenu {

    /**
     * À utiliser lorsque le contexte ne requiert pas de menu.
     */
    AUCUN("aucun"),

    /**
     * Le sous-menu d'un carré.
     */
    CARRE("carre"),

    /**
     * Le sous-menu d'un cercle.
     */
    CERCLE("cercle"),

    /**
     * Le sous-menu d'une ellipse.
     */
    ELLIPSE("ellipse"),

    /**
     * Le sous-menu d'une ligne.
     */
    LIGNE("ligne"),

    /**
     * Le sous-menu d'un rectangle.
     */
    RECTANGLE("rectangle"),

    /**
     * Le sous-menu d'un triangle.
     */
    TRIANGLE("triangle");

    /**
     * Le nom associé à la constante choisie.
     */
    private String nom;

    /**
     * Assigne à {@link #nom} la valeur de la constante choisie.
     *
     * @param nom La valeur de la constante
     */
    EnumMenu(String nom) {
        this.nom = nom;
    }

    /**
     * Retourne le nom du menu en fonction de la constante choisie.
     *
     * @return Le nom du menu
     */
    public String getNom() {
        return nom;
    }

}
