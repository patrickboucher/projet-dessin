package vue;

import modele.CanevasModele;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * La fenêtre principale du programme.
 * <p>
 * Elle contient le menu principal qui permet à l'utilisateur d'ajouter ou de
 * supprimer des formes, et de sauvegarder ou de charger un travail. Elle
 * contient aussi un sous-menu contextuel selon l'outil choisi par
 * l'utilisateur, et un canevas dans lequel les formes sont affichées.
 * <p>
 * Le programme se termine lorsque l'utilisateur ferme cette fenêtre.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class FenetrePrincipale extends JFrame {

    /**
     * La liste de toutes les composantes de cette classe qui peuvent être
     * observées.
     */
    private List<IObservable> observables;

    /**
     * Le canevas dans lequel les formes sont dessinées.
     */
    private CanevasVue canevas;

    /**
     * Crée cette fenêtre principale et toutes ses composantes.
     */
    public FenetrePrincipale(CanevasModele modele) {
        observables = new LinkedList<>();
        setTitle("Projet Dessin");
        setSize(800, 600);
        setLayout(null);
        creeMenuOutils();
        creeCanevas(modele);
        creeMenuContextuel();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    /**
     * Retourne une copie de la liste des {@code IObservable}.
     *
     * @return Copie de la liste des {@code IObservable}
     */
    public Collection<IObservable> getObservables() {
        return List.copyOf(observables);
    }

    /**
     * Retourne la vue {@code CanevasVue} contenue dans cette fenêtre.
     *
     * @return La vue {@code CanevasVue} contenue dans cette fenêtre.
     */
    public CanevasVue getCanevas() {
        return canevas;
    }

    /**
     * Affiche un message dans une nouvelle fenêtre surgissante.
     *
     * @param message Le message à afficher
     */
    public void affiche(String message) {
        JOptionPane.showMessageDialog(this, message);
    }

    /**
     * Pose une question à l'utilisateur qui devra choisir entre oui et non.
     *
     * @param message La question
     * @return "true" si l'utilisateur a choisi "oui"
     */
    public boolean demandeConfirmation(String message) {
        int option = JOptionPane.showConfirmDialog(this, message,
                "Choisir une option", JOptionPane.YES_NO_OPTION);
        return option == JOptionPane.YES_OPTION;
    }

    /**
     * Crée le menu {@code}.
     *
     * @see MenuOutils
     */
    private void creeMenuOutils() {
        PanneauObservable menuOutils = new MenuOutils();
        observables.add(menuOutils);
        add(menuOutils);
    }

    /**
     * Crée le {@code MenuContextuel}.
     *
     * @see MenuContextuel
     */
    private void creeMenuContextuel() {
        MenuContextuel menuContextuel = MenuContextuel.getInstance();
        observables.add(menuContextuel);
        add(menuContextuel);
    }

    /**
     * Crée le {@code CanevasVue} en lui attribuant un {@code CanevasModele}.
     *
     * @param modele Le modèle de la vue
     * @see CanevasVue
     * @see CanevasModele
     */
    private void creeCanevas(CanevasModele modele) {
        canevas = new CanevasVue(modele);
        observables.add(canevas);
        add(canevas);
    }

}
