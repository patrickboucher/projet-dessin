package vue;

import java.awt.event.ActionEvent;

/**
 * Un objet qui peut être observé par des {@link IObservateur}. L'objet
 * observable ajoute des observateurs dans une liste et il doit les informer
 * lorsqu'un événement se produit.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public interface IObservable {

    /**
     * Ajoute un observateur à la liste.
     *
     * @param observateur L'observateur à attacher
     * @see IObservateur
     */
    void attache(IObservateur observateur);

    /**
     * Retire un observateur de la liste.
     *
     * @param observateur L'observateur à détacher
     * @see IObservateur
     */
    void detache(IObservateur observateur);

    /**
     * Informe tous les observateurs de la liste. Cette méthode peut être lancé,
     * par exemple, par un {@link java.awt.event.ActionListener}.
     *
     * @param evenement L'événement créé par l'{@code ActionListener}
     */
    void informeObservateurs(ActionEvent evenement);
}
