package vue;

import java.awt.event.ActionEvent;
import java.util.Collection;

/**
 * Objet qui peut être attaché à un {@link IObservable}. Lorsqu'un événement
 * survient dans un objet observable, une actualisation des données est
 * effectuée par l'observateur.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public interface IObservateur {

    /**
     * Actualise les données des modèles et informe les vues lorsque les données
     * sont traitées.
     *
     * @param evenement  L'événement qui a précédé l'actualisation
     * @param observable L'objet source dans lequel l'événement s'est produit
     */
    void actualise(ActionEvent evenement, IObservable observable);

    /**
     * Attache cet observateur à un {@link IObservable objet observable}.
     *
     * @param observable L'objet à observer
     */
    default void observe(IObservable observable) {
        observable.attache(this);
    }

    /**
     * Attache cet observateur à plusieurs {@link IObservable objets
     * observables}.
     *
     * @param observables La liste des objets à observer
     */
    default void observe(Collection<IObservable> observables) {
        for (IObservable observable : observables) {
            observe(observable);
        }
    }

}
