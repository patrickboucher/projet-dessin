package vue;

import vue.composant.AccesseurChamp;
import vue.composant.CheckBoxPartage;
import vue.composant.IAccesseurChamp;
import vue.composant.SelecteurCouleurPartage;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

/**
 * Menu adaptatif selon le choix de l'outil, qui permet de saisir les données
 * d'une {@code Forme} à dessiner.
 * <p>
 * Utiliser {@link #getInstance()} pour accéder à l'instance de la classe.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class MenuContextuel
        extends PanneauObservable implements IAccesseurChamp {

    /**
     * L'instance de la classe.
     */
    private static MenuContextuel instance;

    /**
     * Contient les sous-menus.
     */
    private CardLayout cardLayout;

    /**
     * Contient les champs texte de tous les sous-menus.
     */
    private AccesseurChamp accesseur;

    /**
     * Ensemble des {@link javax.swing.JCheckBox} partagés entre les cartes qui
     * déterminent si la couleur de fond doit être dessinée.
     */
    private CheckBoxPartage checkBoxForme;

    /**
     * Ensemble des {@link javax.swing.JCheckBox} partagés entre les cartes qui
     * déterminent si la bordure de la forme doit être dessinée.
     */
    private CheckBoxPartage checkBoxBordure;

    /**
     * Ensemble des {@link vue.composant.SelecteurCouleur} partagés entre les
     * cartes pour la couleur de fond de la forme.
     */
    private SelecteurCouleurPartage couleurForme;

    /**
     * Ensemble des {@link vue.composant.SelecteurCouleur} partagés entre les
     * cartes pour la couleur de la bordure de la forme.
     */
    private SelecteurCouleurPartage couleurBordure;

    /**
     * Crée ce menu et tous ses sous-menus.
     */
    private MenuContextuel() {
        accesseur = new AccesseurChamp();
        cardLayout = new CardLayout();
        checkBoxForme = new CheckBoxPartage("Remplissage");
        checkBoxForme.getComposant().setSelected(true);
        checkBoxBordure = new CheckBoxPartage("Contour");
        checkBoxBordure.getComposant().setSelected(true);
        couleurForme = new SelecteurCouleurPartage();
        couleurBordure = new SelecteurCouleurPartage();
        setLayout(cardLayout);
        setBounds(0, 200, 200, 350);
        setVisible(false);
        creeMenuCarre();
        creeMenuRectangle();
        creeMenuCercle();
        creeMenuEllipse();
        creeMenuTriangle();
        creeMenuLigne();
    }

    /**
     * Retourne l'instance de ce menu. L'instanciation  est faite à la première
     * utilisation de cette fonction.
     *
     * @return L'instance de ce menu
     */
    public static MenuContextuel getInstance() {
        if (instance == null) {
            instance = new MenuContextuel();
        }
        return instance;
    }

    /**
     * Retourne la couleur de fond de la forme.
     *
     * @return La couleur de fond de la forme
     */
    public Color getCouleurForme() {
        return couleurForme.getComposant().getCouleur();
    }

    /**
     * Retourne la couleur de la bordure de la forme.
     *
     * @return La couleur de la bordure de la forme
     */
    public Color getCouleurBordure() {
        return couleurBordure.getComposant().getCouleur();
    }

    /**
     * Indique si le {@link javax.swing.JCheckBox} est sélectionné pour dessiner
     * la couleur de fond de la forme.
     *
     * @return "true" si le {@link javax.swing.JCheckBox} est sélectionné
     */
    public boolean formeEstSelectionnee() {
        return checkBoxForme.getComposant().isSelected();
    }

    /**
     * Indique si le {@link javax.swing.JCheckBox} est sélectionné pour dessiner
     * la bordure de la forme.
     *
     * @return "true" si le {@link javax.swing.JCheckBox} est sélectionné
     */
    public boolean bordureEstSelectionnee() {
        return checkBoxBordure.getComposant().isSelected();
    }

    /**
     * {@inheritDoc}
     *
     * @param nomChamp Le nom du champ texte
     * @return La valeur du champ texte qui correspond au nom
     */
    @Override
    public String getTexte(String nomChamp) {
        return accesseur.getTexte(nomChamp);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void effaceChamps() {
        accesseur.effaceChamps();
    }

    /**
     * Affiche le sous-menu désigné par son nom.
     * <p>
     * Le nom des menus est contenu dans {@link EnumMenu}.
     *
     * @param menu Le nom du menu à afficher
     */
    public void affiche(EnumMenu menu) {
        if (menu == EnumMenu.AUCUN) {
            setVisible(false);
        } else {
            cardLayout.show(this, menu.getNom());
            setVisible(true);
        }
    }

    /**
     * Crée le sous-menu qui permet d'entrer les propriétés d'un carré.
     */
    private void creeMenuCarre() {
        JPanel conteneur = new JPanel(new GridBagLayout());
        add(EnumMenu.CARRE.getNom(), conteneur);
        creeSectionTitre(conteneur, "Propriétés du carré");
        String[][] champs = {
                {"X", "carre position x"},
                {"Y", "carre position y"},
                {"Mesure d'un côté", "carre cote"}
        };
        creeSerieChampsTexte(conteneur, champs, 1);
        creeSelecteurCouleur(conteneur, 4, true);
        creeBoutonAppliquer(conteneur, "carre appliquer");
    }

    /**
     * Crée le sous-menu qui permet d'entrer les propriétés d'un rectangle.
     */
    private void creeMenuRectangle() {
        JPanel conteneur = new JPanel(new GridBagLayout());
        add(EnumMenu.RECTANGLE.getNom(), conteneur);
        creeSectionTitre(conteneur, "Propriétés du rectangle");
        String[][] champs = {
                {"X", "rectangle position x"},
                {"Y", "rectangle position y"},
                {"Longueur", "rectangle longueur"},
                {"Hauteur", "rectangle hauteur"}
        };
        creeSerieChampsTexte(conteneur, champs, 1);
        creeSelecteurCouleur(conteneur, 5, true);
        creeBoutonAppliquer(conteneur, "rectangle appliquer");
    }

    /**
     * Crée le sous-menu qui permet d'entrer les propriétés d'un triangle.
     */
    private void creeMenuTriangle() {
        JPanel conteneur = new JPanel(new GridBagLayout());
        add(EnumMenu.TRIANGLE.getNom(), conteneur);
        creeSectionTitre(conteneur, "Propriétés du triangle");
        String[][] champs = {
                {"X1", "triangle x1"},
                {"Y1", "triangle y1"},
                {"X2", "triangle x2"},
                {"Y2", "triangle y2"},
                {"X3", "triangle x3"},
                {"Y3", "triangle y3"}
        };
        creeSerieChampsTexte(conteneur, champs, 1);
        creeSelecteurCouleur(conteneur, 7, true);
        creeBoutonAppliquer(conteneur, "triangle appliquer");
    }

    /**
     * Crée le sous-menu qui permet d'entrer les propriétés d'un cercle.
     */
    private void creeMenuCercle() {
        JPanel conteneur = new JPanel(new GridBagLayout());
        add(EnumMenu.CERCLE.getNom(), conteneur);
        creeSectionTitre(conteneur, "Propriétés du cercle");
        String[][] champs = {
                {"X", "cercle position x"},
                {"Y", "cercle position y"},
                {"Rayon", "cercle rayon"}
        };
        creeSerieChampsTexte(conteneur, champs, 1);
        creeSelecteurCouleur(conteneur, 4, true);
        creeBoutonAppliquer(conteneur, "cercle appliquer");
    }

    /**
     * Crée le sous-menu qui permet d'entrer les propriétés d'un ellipse.
     */
    private void creeMenuEllipse() {
        JPanel conteneur = new JPanel(new GridBagLayout());
        add(EnumMenu.ELLIPSE.getNom(), conteneur);
        creeSectionTitre(conteneur, "Propriétés de l'ellipse");
        String[][] champs = {
                {"X", "ellipse position x"},
                {"Y", "ellipse position y"},
                {"Longueur", "ellipse longueur"},
                {"Hauteur", "ellipse hauteur"}
        };
        creeSerieChampsTexte(conteneur, champs, 1);
        creeSelecteurCouleur(conteneur, 5, true);
        creeBoutonAppliquer(conteneur, "ellipse appliquer");
    }

    /**
     * Crée le sous-menu qui permet d'entrer les propriétés d'une ligne.
     */
    private void creeMenuLigne() {
        JPanel conteneur = new JPanel(new GridBagLayout());
        add(EnumMenu.LIGNE.getNom(), conteneur);
        creeSectionTitre(conteneur, "Propriétés de la ligne");
        String[][] champs = {
                {"X1", "ligne x1"},
                {"Y1", "ligne y1"},
                {"X2", "ligne x2"},
                {"Y2", "ligne y2"}
        };
        creeSerieChampsTexte(conteneur, champs, 1);
        creeSelecteurCouleur(conteneur, 5, false);
        creeBoutonAppliquer(conteneur, "ligne appliquer");
    }

    /**
     * Crée un label titre et le place dans le haut d'un
     * {@link JPanel conteneur}.
     *
     * @param conteneur Le conteneur qui reçoit le titre
     * @param titre     Le texte à afficher
     */
    private void creeSectionTitre(JPanel conteneur, String titre) {
        GridBagConstraints contraintes = new GridBagConstraints();
        contraintes.gridwidth = 2;
        contraintes.fill = GridBagConstraints.HORIZONTAL;
        contraintes.anchor = GridBagConstraints.PAGE_START;
        contraintes.weighty = 0.5;
        conteneur.add(new JLabel(titre), contraintes);
    }

    /**
     * Crée un champ texte et le label qui l'identifie sur une ligne.
     * Le label est positionné dans la première colonne et le champ dans la
     * deuxième colonne. Le nouveau champ devient accessible par
     * {@link #accesseur l'accesseur}.
     *
     * @param conteneur Le conteneur dans lequel créer le champ
     * @param label     Le texte à afficher devant le champ
     * @param nom       Le nom du champ
     * @param ligne     Le numéro de la ligne de grille
     */
    private void creeChampTexte(Container conteneur,
                                String label, String nom, int ligne) {
        GridBagConstraints contraintes = new GridBagConstraints();
        contraintes.anchor = GridBagConstraints.BASELINE_TRAILING;
        contraintes.ipadx = 6;
        contraintes.gridy = ligne;
        conteneur.add(new JLabel(label), contraintes);
        contraintes.anchor = GridBagConstraints.BASELINE_LEADING;
        contraintes.gridx = 1;
        JTextField champ = accesseur.creeChamp(nom);
        champ.setColumns(4);
        conteneur.add(champ, contraintes);
    }

    /**
     * Crée une série de champs textes et leur label, tous sur des lignes
     * différentes, depuis une liste. Les nouveaux champs deviennent accessibles
     * par {@link #accesseur l'accesseur}.
     * <p>
     * La liste doit avoir le format suivant:
     * <p>
     * {@code String[][] liste = {{texteDuLabel, nomDuChamp}, ...}}
     *
     * @param conteneur   Le conteneur dans lequel créer le champ
     * @param champs      La liste des champs et de leur label
     * @param ligneDepart Le numéro de la première ligne de grille
     * @see #creeChampTexte(Container, String, String, int)
     */
    private void creeSerieChampsTexte(Container conteneur,
                                      String[][] champs, int ligneDepart) {
        for (int i = 0; i < champs.length; i++) {
            creeChampTexte(conteneur,
                    champs[i][0], champs[i][1], i + ligneDepart);
        }
    }

    /**
     * Crée le bouton "Appliquer" et le place dans le bas d'un
     * {@link JPanel conteneur}.
     *
     * @param conteneur Le conteneur qui reçoit le bouton
     */
    private void creeBoutonAppliquer(JPanel conteneur, String nomBouton) {
        GridBagConstraints contraintes = new GridBagConstraints();
        contraintes.anchor = GridBagConstraints.PAGE_END;
        contraintes.gridwidth = 2;
        contraintes.weighty = 1;
        contraintes.ipady = 6;
        contraintes.gridy = 99;
        JButton boutonAppliquer = new JButton("Appliquer");
        boutonAppliquer.setName(nomBouton);
        boutonAppliquer.addActionListener(ecouteur);
        conteneur.add(boutonAppliquer, contraintes);
    }

    /**
     * Crée les sélecteurs de couleur et les {@link javax.swing.JCheckBox} et
     * les places dans le conteneur à la ligne désignée. Si la forme ne peut pas
     * avoir de bordures, un seul sélecteur est créé et les checkboxes ne sont
     * pas créés.
     *
     * @param conteneur Le conteneur dans lequel créer les composants
     * @param ligne     Le numéro de la première ligne de la grille
     * @param aBordure  "true" si la forme peut avoir des bordures
     */
    private void creeSelecteurCouleur(JPanel conteneur,
                                      int ligne, boolean aBordure) {
        GridBagConstraints contraintes = new GridBagConstraints();
        contraintes.gridy = ligne;
        contraintes.gridx = 1;
        conteneur.add(couleurForme.duplique(), contraintes);
        if (aBordure) {
            contraintes.gridy = ligne;
            contraintes.gridx = 0;
            conteneur.add(checkBoxForme.duplique(), contraintes);
            contraintes.gridy = ligne + 1;
            conteneur.add(checkBoxBordure.duplique(), contraintes);
            contraintes.gridx = 1;
            conteneur.add(couleurBordure.duplique(), contraintes);
        }
    }

}
