package vue;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.io.IOException;

/**
 * Menu qui permet à l'utilisateur de sélectionner un outil de création pour
 * dessiner des {@code Forme}. Permets aussi de sélectionner l'outil de
 * sauvegarde du dessin.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class MenuOutils extends PanneauObservable {

    /**
     * Crée ce menu.
     */
    public MenuOutils() {
        setLayout(new GridBagLayout());
        setBounds(0, 0, 200, 200);
        setBackground(Color.LIGHT_GRAY);
        creeBouton("Ouvrir", "/img/ouvrir.png", 0, 0);
        creeBouton("Sauvegarder", "/img/sauvegarde.png", 1, 0);
        creeBouton("Supprimer sélection", "/img/supprime.png", 2, 0);
        creeBouton("Supprimer tout", "/img/supprime_tout.png", 3, 0);
        creeBouton("Ligne", "/img/ligne.png", 0, 1);
        creeBouton("Carré", "/img/carre.png", 1, 1);
        creeBouton("Rectangle", "/img/rectangle.png", 2, 1);
        creeBouton("Triangle", "/img/triangle.png", 3, 1);
        creeBouton("Cercle", "/img/cercle.png", 4, 1);
        creeBouton("Ellipse", "/img/ellipse.png", 5, 1);
    }

    /**
     * Crée un bouton avec une icône dans le {@link GridBagLayout}. Le nom du
     * bouton est affiché si l'image n'existe pas.
     *
     * @param nom      Le nom du bouton
     * @param imageURL Le lien vers l'image
     * @param colonne  Le numéro de colonne dans la grille
     * @param rangee   Le numéro de rangée dans la grille
     */
    private void creeBouton(String nom,
                            String imageURL, int colonne, int rangee) {
        JButton bouton = new JButton();
        GridBagConstraints contraintes = new GridBagConstraints();
        contraintes.gridx = colonne;
        contraintes.gridy = rangee;
        contraintes.insets = new Insets(6, 1, 0, 1);
        add(bouton, contraintes);
        try {
            Image image = ImageIO.read(getClass().getResource(imageURL));
            bouton.setIcon(new ImageIcon(image));
        } catch (IllegalArgumentException exception) {
            String message = imageURL + " : aucun fichier.";
            System.err.println(message);
        } catch (IOException exception) {
            System.err.println(exception.getMessage());
        } finally {
            if (bouton.getIcon() == null) {
                bouton.setText(nom);
            } else {
                bouton.setToolTipText(nom);
            }
            bouton.setName(nom);
            bouton.setMargin(new Insets(0, 0, 0, 0));
            bouton.addActionListener(ecouteur);
        }
    }

}
