package vue;

import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

/**
 * Un {@link JPanel panneau} qui peut être attaché à un
 * {@link javax.swing.JComponent} et observé par un objet {@link IObservateur}.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 * @see IObservable
 */
public abstract class PanneauObservable extends JPanel implements IObservable {

    /**
     * La liste des objets qui observent cette classe.
     */
    protected List<IObservateur> observateurs;

    /**
     * L'écouteur d'événements. Il doit être lié aux composants de la classe et
     * informer les observateurs lorsqu'un événement survient.
     */
    protected ActionListener ecouteur;

    /**
     * Crée ce panneau.
     */
    protected PanneauObservable() {
        observateurs = new LinkedList<>();
        ecouteur = evenement -> informeObservateurs(evenement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void attache(IObservateur observateur) {
        if (observateur != null) {
            if (!observateurs.contains(observateur)) {
                observateurs.add(observateur);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void detache(IObservateur observateur) {
        observateurs.remove(observateur);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void informeObservateurs(ActionEvent evenement) {
        observateurs.forEach(observateur
                -> observateur.actualise(evenement, this));
    }

}
