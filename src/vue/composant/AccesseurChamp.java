package vue.composant;

import javax.management.openmbean.KeyAlreadyExistsException;
import javax.swing.JTextField;
import java.util.Hashtable;

/**
 * Liste de {@link JTextField champs texte} accessibles par leur nom. Permets
 * d'accéder rapidement à leur contenu en spécifiant seulement leur nom.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class AccesseurChamp {

    /**
     * La liste des champs.
     */
    private Hashtable<String, JTextField> champs;

    /**
     * Crée l'accesseur de champ.
     */
    public AccesseurChamp() {
        champs = new Hashtable<>();
    }

    /**
     * Crée un champ texte dans la liste et retourne ce nouveau champ. Si un
     * champ existe déjà sous ce nom, ce champ est retourné.
     * Le nom est assigné à {@code JTextField.name} du champ texte.
     *
     * @param nom Le nom du champ texte
     * @return Le nouveau champ ou le champ qui utilise le nom
     * @throws NullPointerException Si le nom est null.
     */
    public JTextField creeChamp(String nom) {
        JTextField champ = new JTextField();
        champ.setName(nom);
        if (!champs.containsKey(nom)) {
            ajoute(champ, nom);
        } else {
            champ = champs.get(nom);
        }
        return champ;
    }

    /**
     * Ajoute un champ texte à la liste en lui attribuant une clé qui correspond
     * à son nom.
     *
     * @param champ Le champ texte à ajouter
     * @param nom   Le nom du champ texte
     * @throws NullPointerException      Si le nom est null.
     * @throws KeyAlreadyExistsException Si le nom est déjà utilisé pour un
     *                                   champ texte différent.
     */
    public void ajoute(JTextField champ, String nom)
            throws KeyAlreadyExistsException {
        if (!champs.containsKey(nom)) {
            champs.put(nom, champ);
        } else {
            if (!champs.get(nom).equals(champ)) {
                throw new KeyAlreadyExistsException("La clé '" + nom +
                        "' existe déjà pour un autre champ texte.");
            }
        }
    }

    /**
     * Retire de la liste le champ texte correspondant au nom.
     *
     * @param nom Le nom du champ texte
     */
    public void retire(String nom) {
        if (nom != null) {
            champs.remove(nom);
        }
    }

    /**
     * Retourne de la liste l'objet {@link JTextField} qui correspond au nom.
     *
     * @param nom Le nom du champ texte
     * @return Le champ texte qui correspond au nom
     */
    public JTextField getChamp(String nom) {
        JTextField resultat = null;
        if (nom != null) {
            resultat = champs.get(nom);
        }
        return resultat;
    }

    /**
     * Retourne la valeur du champ texte de la liste qui correspond au nom.
     *
     * @param nomChamp Le nom du champ texte
     * @return La valeur du champ texte qui correspond au nom
     */
    public String getTexte(String nomChamp) {
        String resultat = "";
        if (nomChamp != null) {
            if (champs.containsKey(nomChamp)) {
                resultat = champs.get(nomChamp).getText();
            }
        }
        return resultat;
    }

    /**
     * Efface le texte de tous les champs.
     */
    public void effaceChamps() {
        champs.values().forEach(champ -> champ.setText(""));
    }

}
