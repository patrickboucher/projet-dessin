package vue.composant;

import javax.swing.JCheckBox;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Un {@link JCheckBox} qui peut être dupliqué. Seul le texte est dupliqué.
 * Le parent du checkbox dupliqué est null. Tous les checkbox sont synchronisés
 * lorsque la valeur {@link JCheckBox#isSelected()} change.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class CheckBoxPartage
        extends Partage<JCheckBox> implements ItemListener {

    /**
     * Crée un nouveau {@link JCheckBox} et l'ajoute dans la liste des
     * composants synchronisés.
     *
     * @param texte Le texte à afficher à droite du {@link JCheckBox}
     */
    public CheckBoxPartage(String texte) {
        super(new JCheckBox(texte));
    }

    /**
     * {@inheritDoc}
     *
     * @return La copie synchronisée du {@link JCheckBox}
     */
    @Override
    public JCheckBox duplique() {
        JCheckBox nouveau = new JCheckBox();
        nouveau.setText(composant.getText());
        nouveau.setSelected(composant.isSelected());
        synchronise(nouveau);
        return nouveau;
    }

    /**
     * {@inheritDoc}
     *
     * @param composant Le {@link JCheckBox} à synchroniser
     */
    @Override
    protected void synchronise(JCheckBox composant) {
        composant.addItemListener(this);
        composantsSynchrones.add(composant);
    }

    /**
     * {@inheritDoc}
     *
     * @param evenement L'événement à traiter
     */
    @Override
    public void itemStateChanged(ItemEvent evenement) {
        JCheckBox source = (JCheckBox) evenement.getSource();
        composantsSynchrones.forEach(checkBox ->
                checkBox.setSelected(source.isSelected()));
    }
}
