package vue.composant;

/**
 * Interface qui permet à un autre objet d'accéder au contenu des
 * {@link javax.swing.JTextField champs texte} de cette classe.
 * <p>
 * Pour que ces méthodes puissent fonctionner, {@link #getTexte(String)} doit
 * être lié à la liste des champs et retourner la valeur de
 * {@link AccesseurChamp#getTexte(String)}.
 * <p>
 * Exemple:
 * <pre>{@code
 * public class Vue implements IAccesseurChamp {
 *     private AccesseurChamp accesseur = new AccesseurChamp();
 *
 *     @Override
 *     public String getTexte(String nomChamp) {
 *         return accesseur.getTexte(nomChamp);
 *     }
 * }
 * }</pre>
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 * @see AccesseurChamp
 */
public interface IAccesseurChamp {

    /**
     * Retourne la valeur du champ texte de la liste qui correspond au nom.
     *
     * @param nomChamp Le nom du champ texte
     * @return La valeur du champ texte qui correspond au nom
     */
    String getTexte(String nomChamp);

    /**
     * Retourne la valeur du champ texte de la liste qui correspond au nom,
     * convertie en nombre à virgule.
     * Si la valeur ne peut être convertie, retourne 0.
     *
     * @param nomChamp Le nom du champ texte
     * @return La valeur du champ texte qui correspond au nom, en nombre réel,
     * ou 0.0
     */
    default double getReel(String nomChamp) {
        double resultat = 0.0;
        try {
            resultat = Double.parseDouble(getTexte(nomChamp));
        } catch (NumberFormatException exception) {
            String message = "Champ " +
                    nomChamp + " : " + exception.getMessage();
            System.err.println(message);
        }
        return resultat;
    }

    /**
     * Retourne la valeur du champ texte de la liste qui correspond au nom,
     * convertie en nombre à entier.
     * Si la valeur ne peut être convertie, retourne 0.
     *
     * @param nomChamp Le nom du champ texte
     * @return La valeur du champ texte qui correspond au nom, en nombre entier,
     * ou 0
     */
    default int getEntier(String nomChamp) {
        return (int) getReel(nomChamp);
    }

    /**
     * Efface le texte de tous les champs.
     */
    void effaceChamps();

}
