package vue.composant;

import java.util.LinkedList;
import java.util.List;

/**
 * Un composant qui peut être dupliqué. Les propriétés des duplications sont
 * synchronisées avec le composant d'origine. Utile si, par exemple, un
 * composant doit être partagé par plusieur parents, mais qu'un seul parent ne
 * peut être assigné à un composant.
 *
 * @param <Type> Le type du composant
 * @author Patrick Boucher
 * @version %I%, %G$
 */
public abstract class Partage<Type> {

    /**
     * Le composant à partager.
     */
    protected Type composant;

    /**
     * La liste des composants synchronisés.
     */
    protected List<Type> composantsSynchrones;

    /**
     * Assigne le composant et l'ajoute dans la liste des composants
     * synchronisés.
     *
     * @param composant Le composant d'origine
     */
    public Partage(Type composant) {
        this.composant = composant;
        composantsSynchrones = new LinkedList<>();
        synchronise(composant);
    }

    /**
     * Retourne le composant d'origine.
     *
     * @return Le composant d'origine
     */
    public Type getComposant() {
        return composant;
    }

    /**
     * Crée une copie synchronisée du composant.
     *
     * @return La copie synchronisée du composant
     */
    public abstract Type duplique();

    /**
     * Ajoute l'action de synchronisation à un composant et ajoute ce dernier à
     * la liste des composants synchonisés.
     *
     * @param composant Le composant à synchroniser
     */
    protected abstract void synchronise(Type composant);

}
