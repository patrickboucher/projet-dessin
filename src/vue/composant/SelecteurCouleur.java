package vue.composant;

import vue.IObservable;
import vue.IObservateur;

import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;

/**
 * Un rectangle qui affiche une couleur. Lorsque l'utilisateur clique dessus, un
 * {@link JColorChooser} ouvre pour sélectionner une nouvelle couleur.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class SelecteurCouleur extends JPanel implements IObservable {

    /**
     * Liste des {@link IObservateur} qui écoutent si un changement de couleur
     * se produit.
     */
    private List<IObservateur> observateurs;

    /**
     * La couleur sélectionnée.
     */
    private Color couleur;

    /**
     * Crée le sélecteur et assigne une couleur par défaut.
     */
    public SelecteurCouleur() {
        observateurs = new LinkedList<>();
        couleur = Color.BLACK;
        setBackground(couleur);
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setPreferredSize(new Dimension(16, 16));
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                selectionneCouleur();
            }
        });
    }

    /**
     * Retourne la couleur.
     *
     * @return La couleur
     */
    public Color getCouleur() {
        return couleur;
    }

    /**
     * Assigne la couleur. La couleur de fond du rectangle est changé pour
     * correspondre.
     *
     * @param couleur La couleur à assigner
     */
    public void setCouleur(Color couleur) {
        if (couleur == null) {
            throw new NullPointerException("La couleur ne peut pas être null");
        }
        this.couleur = couleur;
        setBackground(couleur);
    }

    /**
     * Ouvre le {@link JColorChooser}.
     */
    private void selectionneCouleur() {
        Color nouvelleCouleur = JColorChooser.showDialog(this,
                "Sélectionner une couleur", couleur);
        if (nouvelleCouleur != null) {
            setCouleur(nouvelleCouleur);
            informeObservateurs(new ActionEvent(this, 0, null));
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param observateur L'observateur à attacher
     */
    @Override
    public void attache(IObservateur observateur) {
        if (observateur != null) {
            if (!observateurs.contains(observateur)) {
                observateurs.add(observateur);
            }
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param observateur L'observateur à détacher
     */
    @Override
    public void detache(IObservateur observateur) {
        observateurs.remove(observateur);
    }

    /**
     * Informe les observateurs qu'une nouvelle couleur a été sélectionnée.
     *
     * @param evenement L'événement créé par l'{@code ActionListener}
     */
    @Override
    public void informeObservateurs(ActionEvent evenement) {
        observateurs.forEach(observateur ->
                observateur.actualise(evenement, this));
    }
}
