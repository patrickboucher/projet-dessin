package vue.composant;

import vue.IObservable;
import vue.IObservateur;

import java.awt.event.ActionEvent;

/**
 * Un {@link SelecteurCouleur} qui peut être dupliqué. La couleur des sélecteurs
 * est synchronisée lorsque leur valeur change.
 *
 * @author Patrick Boucher
 * @version %I%, %G%
 */
public class SelecteurCouleurPartage
        extends Partage<SelecteurCouleur> implements IObservateur {

    /**
     * Crée un nouveau {@link SelecteurCouleur} et l'ajoute dans la liste des
     * composants synchronisés.
     */
    public SelecteurCouleurPartage() {
        super(new SelecteurCouleur());
    }

    /**
     * {@inheritDoc}
     *
     * @return La copie synchronisée du {@link SelecteurCouleur}
     */
    @Override
    public SelecteurCouleur duplique() {
        SelecteurCouleur nouveau = new SelecteurCouleur();
        nouveau.setCouleur(composant.getCouleur());
        synchronise(nouveau);
        return nouveau;
    }

    /**
     * {@inheritDoc}
     *
     * @param composant Le {@link SelecteurCouleur} à synchroniser
     */
    @Override
    protected void synchronise(SelecteurCouleur composant) {
        composant.attache(this);
        composantsSynchrones.add(composant);
    }

    /**
     * Action déclenché qui transmet la nouvelle couleur du composant modifié
     * aux composants de la liste {@link #composantsSynchrones}.
     *
     * @param evenement  L'événement qui a précédé l'actualisation
     * @param observable L'objet source dans lequel l'événement s'est produit
     */
    @Override
    public void actualise(ActionEvent evenement, IObservable observable) {
        SelecteurCouleur source = (SelecteurCouleur) observable;
        composantsSynchrones.forEach(selecteur ->
                selecteur.setCouleur(source.getCouleur()));
    }

}
